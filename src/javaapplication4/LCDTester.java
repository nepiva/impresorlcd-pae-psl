
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class LCDTester {
    
    public static void main(String[] args) {

        // Establece los segmentos de cada numero
        List<String> listaComando = new ArrayList<>();
        String comando;        
        int espacioDig = 0;
        String CADENA_FINAL = "0,0";
        String[] parametros;        
        int tam;
        int valor;
        
        try {
            Scanner lector = new Scanner(System.in);
                
            System.out.print("Espacio entre Digitos (0 a 5): ");
            comando = lector.next();

            // Valida si es un numero
            try {                
                espacioDig = Integer.parseInt(comando);
                // se valida que el espaciado este entre 0 y 5
                ValidarRangoEspacio(espacioDig);
            } 
            catch(ExcepcionIntervalo ex){
                ex.getMessage();
            }catch (NumberFormatException ex){
                throw new IllegalArgumentException("No es entero: "+ex.getMessage());
            }           

            do
            {
                System.out.print("Entrada: ");
                comando = lector.next();
                if(!comando.equalsIgnoreCase(CADENA_FINAL))
                {                                       
                    try {
                        //Se hace el split de la cadena
                        parametros = comando.split(",");

                        //Valida la cantidad de parametros
                        if(parametros.length>2)
                        {
                            throw new IllegalArgumentException("Cadena " + comando
                                    + " contiene mas caracter ,"); 
                        }

                        //Valida la cantidad de parametros
                        if(parametros.length<2)
                        {
                           throw new IllegalArgumentException("Cadena " + comando
                                    + " no contiene los parametros requeridos"); 
                        }

                        //Valida que el parametro size y el numero a mostrar sea numerico
                        try {
                            tam = Integer.parseInt(parametros[0]);
                            valor = Integer.parseInt(parametros[1]);

                            // se valida que el size este entre 1 y 10
                            ValidarRangoSize(tam);
                        } 
                        catch(ExcepcionIntervalo ex){
                            ex.getMessage();
                        }catch (NumberFormatException ex){
                            throw new IllegalArgumentException("Ambos valores deben ser numeros enteros");
                        }

                        //Si es valid la cadena se agrega a la lista
                        listaComando.add(comando);
                    
                    }catch (Exception ex){
                        System.out.println(ex.getMessage());
                    }
                }
            }while (!comando.equalsIgnoreCase(CADENA_FINAL));             

            ImpresorLCD impresorLCD = new ImpresorLCD();

            Iterator<String> iterator = listaComando.iterator();
            while (iterator.hasNext()) 
            {
                try 
                {
                    parametros = iterator.next().split(",");        
                    tam = Integer.parseInt(parametros[0]);
                    
                    impresorLCD.imprimirNumero(tam, parametros[1], espacioDig);

                } catch (Exception ex) 
                {
                    System.out.println("Error: "+ex.getMessage());
                }
            }
        } catch (Exception ex) 
        {
            System.out.println("Error: "+ex.getMessage());
        }

    }
    
    
    static void ValidarRangoEspacio(int num)throws ExcepcionIntervalo{
        if(num <0 || num >5)
            {
                throw new IllegalArgumentException("El espacio entre "
                        + "digitos debe estar entre 0 y 5");
            }
    }
     static void ValidarRangoSize(int num)throws ExcepcionIntervalo{
        if(num <0 || num >10)
            {
                throw new IllegalArgumentException("El parametro size "
                        + "debe estar entre 0 y 10");
            }
    }  


}
